# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Warning: this is actually a product definition, to be inherited from

PRODUCT_PACKAGES := \
    DroidSansMono.ttf \
    AndroidClock.ttf \
    fonts.xml

PRODUCT_COPY_FILES += \
    frameworks/base/data/fonts/GoogleSans-Bold.ttf:system/fonts/GoogleSans-Bold.ttf \
    frameworks/base/data/fonts/GoogleSans-BoldItalic.ttf:system/fonts/GoogleSans-BoldItalic.ttf \
    frameworks/base/data/fonts/GoogleSans-Italic.ttf:system/fonts/GoogleSans-Italic.ttf \
    frameworks/base/data/fonts/GoogleSans-Medium.ttf:system/fonts/GoogleSans-Medium.ttf \
    frameworks/base/data/fonts/GoogleSans-MediumItalic.ttf:system/fonts/GoogleSans-MediumItalic.ttf \
    frameworks/base/data/fonts/GoogleSans-Regular.ttf:system/fonts/GoogleSans-Regular.ttf
